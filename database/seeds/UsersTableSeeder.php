<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
          'name' => 'Lektorius',
          'surname' => 'Lektorius',
          'phone_number' => '+370 630 55925',
          'birthdate' => '2016-11-22',
          'email' => 'info@3wa.lt',
          'password' => bcrypt('password'),
          'address' => 'Ševčenkos g. 8',
          'city' => 'Vilnius',
          'zip_code' => 'LT-08382',
          'country' => 'Lithuania'
        ]);
    }
}
