<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $menus = \App\Menu::all();

    return view('welcome', compact('menus'));
})->name('homepage');

Route::get('/contacts', function() {
  $id = 1;
  $contacts = \App\Contacts::find($id);

  return view('contacts.index', compact('contacts'));

})->name('contacts');
