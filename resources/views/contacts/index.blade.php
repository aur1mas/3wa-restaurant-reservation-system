@extends('layouts.app')

@section('title') Kontaktai @endsection

@section('content')
  <h1>{{ $contacts->name }}</h1>

  <h2>Darbo valandos</h2>
  <p>{!! $contacts->working_hours !!}</p>

  <h2>Papildoma informacija</h2>
  <p>{{ $contacts->info }}</p>

  <div id="map" style="height: 400px; width: 80%;"></div>
  <script>
    function initMap() {
      var uluru = {lat: {{ $contacts->latitude }}, lng: {{ $contacts->longitude }} };
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 4,
        center: uluru
      });
      var marker = new google.maps.Marker({
        position: uluru,
        map: map
      });
    }
  </script>
  <script async defer
  src="https://maps.googleapis.com/maps/api/js?callback=initMap&key=AIzaSyDuihE6DTENsn1dsEMWQ_On-y12PmuRt_M">
  </script>
@endsection
