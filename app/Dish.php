<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
    public function menu()
    {
        return $this->belongsTo('App\Menu');
    }

    public function carts()
    {
        return $this->belongsToMany('App\Cart');
    }

    public function orders()
    {
        return $this->hasManyThrough('App\Order', 'App\OrderItem');
    }
}
